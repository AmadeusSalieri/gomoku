#!/usr/bin/env python3

import os

if os.name == 'nt':
	os.system('del /s build')
else:
	os.system('rm -rf build')

os.system('mkdir build')
os.chdir('./build')
os.system('cmake ..')
os.system('cmake --build .')
