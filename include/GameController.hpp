#ifndef CONTROLLER_HPP_
#define CONTROLLER_HPP_

#include "CmdHandler.hpp"
#include "Board.hpp"
#include "Parser.hpp"
#include "IO.hpp"
#include "AI.hpp"
#include <memory>

class Parser;
class CmdHandler;

class GameController
{
public:
	GameController();
	~GameController();

	Board &getBoard() const;
	Parser &getParser() const;
	AI &getAI() const;

	void runCommand(std::string &command);
	void newBoard(size_t size);
	bool isReady = false;

	std::unique_ptr<IO> io;

private:
	std::shared_ptr<Board> _board;
	std::unique_ptr<Parser> _parser;
	std::unique_ptr<CmdHandler> _cmdHandler;
	std::unique_ptr<AI> _AI;
};

#endif /*CONTROLLER_HPP_*/