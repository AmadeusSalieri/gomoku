#ifndef PARSER_HPP_
#define PARSER_HPP_

#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <vector>
#include <iterator>

class Parser
{
public:
	Parser();
	static std::vector<std::string> split(const std::string &s, char delim);
	template<typename Out> static void split(const std::string &s, char delim, Out result);

};

#endif /* PARSER_HPP */