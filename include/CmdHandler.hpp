#ifndef BABEL_CMDHANDLER_HPP
#define BABEL_CMDHANDLER_HPP

#include "GameController.hpp"
#include <iostream>
#include <vector>

class GameController;

class CmdHandler {
public:

    CmdHandler();

	static size_t info(GameController &controller, std::vector<std::string> &words);
	static size_t begin(GameController &controller, std::vector<std::string> &words);
	static size_t start(GameController &c, std::vector<std::string> &words);
	static size_t turn(GameController &c, std::vector<std::string> &words);
	static size_t end(GameController &controller, std::vector<std::string> &words);
	static size_t restart(GameController &controller, std::vector<std::string> &words);

	void runCommand(std::string &command, GameController &controller);

private:
	std::vector<size_t(*)(GameController &, std::vector<std::string> &)> _functions;
	std::vector<std::string> _keys;

};

#endif //BABEL_CMDHANDLER_HPP
