//
// Created by Kazuhiro on 09/10/2018.
//

#ifndef BABEL_IO_HPP
#define BABEL_IO_HPP

#include <string>
#include <fstream>
#include <iostream>

class IO {
public:

    IO() = default;
    explicit IO(std::string &data);

    const std::string &readInput();
	void out(const std::string &message);
	void setFile(const std::string &file);
	void writeBrain(const std::string &message);
	void writeManager(const std::string &message);
	void closeFile();

private:
    std::string _data;
	std::ofstream _file;
};

#endif //BABEL_IO_HPP
