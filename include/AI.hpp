//
// Created by Kazuhiro on 27/10/2018.
//

#ifndef GOMOKU_AI_HPP
#define GOMOKU_AI_HPP

#include "Board.hpp"
#include "Position.hpp"
#include "Graph.hpp"
#include <vector>
#include <memory>

struct Pattern {
	Pattern() : patternBoard(Board(1)), position(-1, -1), score(0) {}
	Pattern(const Board &pB, Position p, size_t sco) : patternBoard(pB), position(p), score(sco) {}
	Board patternBoard;
	Position position;
	size_t score;
};

class AI {
public:

	explicit AI(std::shared_ptr<Board> board);

	Position play();

	void setBoard(std::shared_ptr<Board> newBoard);

	enum pawnState {
		IGNORE,
		ALLY,
		ENEMY,
		EMPTY,
		VACANT,
	};

private:

	bool patternMatches(Board &board, Position &position, bool enemy = false);

	std::vector<Pattern> searchMatchingPattern();
	std::vector<Pattern> searchMatchingPattern(Board &board, bool enemy = false);

	Position simulateEnemy(Board &board);
	std::vector<Pattern> simulate(Board &board);

	void computePatterns();
	void generateGraph();

	size_t getBestBranch(Node &node);

	std::unique_ptr<Node> generateGraph(Board &board, size_t score = 0,
			size_t deepness = 1, Position posPlayed = Position(-1, -1));
	std::unique_ptr<Node> _root;
	std::shared_ptr<Board> _board;
	std::vector<Board> _patterns;
};

#endif //GOMOKU_AI_HPP
