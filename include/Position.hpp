//
// Created by Kazuhiro on 28/10/2018.
//

#ifndef GOMOKU_POSITION_HPP
#define GOMOKU_POSITION_HPP

#include <cstdlib>

struct Position {
	Position() : x(0), y(0) {}
	Position(int a, int b) : x(a), y(b) {}
	int x;
	int y;
};

#endif //GOMOKU_POSITION_HPP
