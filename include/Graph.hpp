//
// Created by Kazuhiro on 29/10/2018.
//

#ifndef GOMOKU_GRAPH_HPP
#define GOMOKU_GRAPH_HPP

#include "Board.hpp"
#include <memory>
#include <map>

struct Node {
	Node() : board(Board(1)) {}
	explicit Node(size_t boardSize) : board(Board(boardSize)) {}
	explicit Node(Board &b) : board(b) {}

	Board board;
	Position playedPosition;
	size_t score;
	std::vector<std::unique_ptr<Node>> next;
};

class Graph {
public:
	explicit Graph(Board root);
	~Graph() = default;

	void addNode(std::unique_ptr<Node> parent, std::unique_ptr<Node> child);
	void deleteNode(std::unique_ptr<Node> node);

private:
	std::unique_ptr<Node> _root;
	size_t _score;
};

#endif //GOMOKU_GRAPH_HPP
