#ifndef BOARD_HPP_
#define BOARD_HPP_

#include <iostream>
#include <vector>
#include "Position.hpp"

class Board
{
public:
	Board(int size, std::string board);
	explicit Board(int size);
	~Board();
	size_t getSize() const;
	Position getMove() const;
	std::vector<Position> getPawns() const;
	void setBox(const Position &position, int value);
	void setBoxSave(Position position, int value);
	int getBox(const Position &position) const;
	Board turnBoard();

private:
	const size_t _sizeBoard;
	std::vector<int> _array;
	std::vector<Position> _pawns;
};

#endif /*BOARD_HPP*/