#include <IO.hpp>
#include <iostream>
#include "CmdHandler.hpp"
#include "GameController.hpp"

int main(int ac, char **av) {
	GameController c;
	std::string log;

	c.io->setFile("./log.txt");
	while (true) {
		log = c.io->readInput();
		if (log == "END")
			break;
		c.io->writeManager(log);
		c.runCommand(log);
	}
    return EXIT_SUCCESS;
}
