#include <memory>

#include "GameController.hpp"

GameController::GameController() : io(std::make_unique<IO>()), _board(std::make_shared<Board>(20)),
_parser(std::make_unique<Parser>()), _cmdHandler(std::make_unique<CmdHandler>()),
_AI(std::make_unique<AI>(_board))
{
}

GameController::~GameController() = default;

void GameController::runCommand(std::string &command)
{
	GameController &ref = *this;
	_cmdHandler->runCommand(command, ref);
}

void GameController::newBoard(const size_t size)
{
	_board = std::make_shared<Board>(size);
	_AI->setBoard(_board);
}

Board &GameController::getBoard() const
{
	return *_board;
}

Parser &GameController::getParser() const
{
	return *_parser;
}

AI &GameController::getAI() const
{
	return *_AI;
}
