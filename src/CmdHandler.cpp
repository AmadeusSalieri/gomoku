//
// Created by Kazuhiro on 09/10/2018.
//

#include "CmdHandler.hpp"

CmdHandler::CmdHandler() : _functions({&info, &start, &end, &turn, &begin, &restart}),
_keys({"INFO", "START", "END", "TURN", "BEGIN", "RESTART"})
{
}

size_t CmdHandler::info(GameController &controller, std::vector<std::string> &words)
{
	if (words.size() < 2)
		return EXIT_FAILURE;
	return EXIT_SUCCESS;
}

size_t CmdHandler::begin(GameController &controller, std::vector<std::string> &words)
{
	Board &b = controller.getBoard();

	controller.isReady = true;
	auto x = std::rand() % controller.getBoard().getSize();
	auto y = std::rand() % controller.getBoard().getSize();

	controller.io->out(std::to_string(x) + "," + std::to_string(y));
	controller.io->writeBrain(std::to_string(x) + "," + std::to_string(y));
	b.setBoxSave(Position(x, y), 1);
	return EXIT_SUCCESS;
}

size_t CmdHandler::start(GameController &c, std::vector<std::string> &words)
{
	if (words.size() < 2) {
		return EXIT_FAILURE;
	}
	int sizeBoard = std::stoi(words[1]);

	c.io->out("OK");
	c.io->writeBrain("OK");
	if (sizeBoard > 0)
		c.newBoard(static_cast<const size_t>(sizeBoard));
	return EXIT_SUCCESS;
}

size_t CmdHandler::restart(GameController &c, std::vector<std::string> &words)
{
	c.newBoard(c.getBoard().getSize());
	c.isReady = false;
	c.io->out("OK");
	c.io->writeBrain("OK");
	return EXIT_SUCCESS;
}

size_t CmdHandler::turn(GameController &c, std::vector<std::string> &words)
{
	if (words.size() < 2) {
		return EXIT_FAILURE;
	}
	std::string log;
	Board &board = c.getBoard();
	AI &ai = c.getAI();
	Position posAI;
	c.isReady = true;
	std::vector<std::string> numbers = Parser::split(words[1], ',');
	int x = std::stoi(numbers[0]);
	int y = std::stoi(numbers[1]);
	log = "setbox(" + std::to_string(x) + ", " + std::to_string(y) + ")";
	c.io->writeManager(log);
	board.setBoxSave(Position(x, y), 2);
	posAI = ai.play();
	log = "setbox(" + std::to_string(posAI.x) + ", " + std::to_string(posAI.y) + ")";
	c.io->writeBrain(log);
	c.io->out(std::to_string(posAI.x) + "," + std::to_string(posAI.y));
	board.setBoxSave(Position(posAI.x, posAI.y), 1);
	return EXIT_SUCCESS;
}

size_t CmdHandler::end(GameController &controller, std::vector<std::string> &words)
{
	controller.io->writeBrain("Ending");
	exit(EXIT_SUCCESS);
}

void CmdHandler::runCommand(std::string &command, GameController &controller)
{
	std::vector<std::string> words = Parser::split(command, ' ');

	for (size_t i = 0; i < _functions.size(); i++) {
		if (words.empty()) {
			return;
		}
		if (std::strcmp(words[0].c_str(), _keys[i].c_str()) == 0) {
			size_t(*fct)(GameController &, std::vector<std::string> &) = _functions[i];
			(*fct)(controller, words);
		}
	}
}
