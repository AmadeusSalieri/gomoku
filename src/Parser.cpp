#include "Parser.hpp"

Parser::Parser()
{
}

/*
** The key can be:
** timeout_turn  - time limit for each move (milliseconds, 0=play as fast as possible)
** timeout_match - time limit of a whole match (milliseconds, 0=no limit)
** max_memory    - memory limit (bytes, 0=no limit)
** time_left     - remaining time limit of a whole match (milliseconds)
** game_type     - 0=opponent is human, 1=opponent is brain, 2=tournament, 3=network tournament
** rule          - bitmask or sum of 1=exactly five in a row win, 2=continuous game, 4=renju
** evaluate      - coordinates X,Y representing current position of the mouse cursor
** folder        - folder for persistent files
*/

template<typename Out>
void Parser::split(const std::string &s, char delim, Out result) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		*(result++) = item;
	}
}

std::vector<std::string> Parser::split(const std::string &s, char delim)
{
	std::vector<std::string> elems;
	Parser::split(s, delim, std::back_inserter(elems));
	return elems;
}

