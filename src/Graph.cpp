//
// Created by Kazuhiro on 29/10/2018.
//

#include <Graph.hpp>

#include "Graph.hpp"

Graph::Graph(Board root) : _root(std::make_unique<Node>(root))
{

}

void Graph::addNode(std::unique_ptr<Node> parent, std::unique_ptr<Node> child)
{
	parent->next.push_back(std::move(child));
}

void Graph::deleteNode(std::unique_ptr<Node> node)
{
	Node *nodeP = node.release();
	delete nodeP;
}
