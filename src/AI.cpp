#include <utility>

//
// Created by Kazuhiro on 27/10/2018.
//

#include <AI.hpp>

#include "AI.hpp"

AI::AI(std::shared_ptr<Board> board) : _root(std::make_unique<Node>(*board)), _board(std::move(board))
{
	computePatterns();
}

bool AI::patternMatches(Board &board, Position &position, const bool enemy)
{
	Position origin(position.x - 2, position.y - 2);
	for (size_t y = 0; y < 5; y++) {
		for (size_t x = 0; x < 5; x++) {

			size_t patternBox = board.getBox(Position(x, y));
			Position boardPos(origin.x + x, origin.y + y);
			int boardBox;

			if (patternBox == IGNORE)
				continue;

			if ((boardPos.x < 0 || boardPos.y < 0) || \
			(static_cast<size_t>(boardPos.x) >= _board->getSize() || static_cast<size_t>(boardPos.y) >= _board->getSize()))
			{
				return false;
			}

			boardBox = _board->getBox(boardPos);

			if (boardBox == 0 && (patternBox == EMPTY || patternBox == VACANT))
				continue;

			if ((boardBox != patternBox && !enemy) || (boardBox == patternBox && enemy)) {
				return false;
			}
		}
	}
	return true;
}

void AI::computePatterns()
{
	static const std::vector<std::string> patterns = {
			//4 allies
			"00000" "00000" "11114" "00000" "00000",
			"00000" "00000" "11141" "00000" "00000",
			"10000" "01000" "00100" "00010" "00004",
			"10000" "01000" "00100" "00040" "00001",
			//Deadly threats
			"00000" "00000" "22224" "00000" "00000",
			"00000" "00000" "22242" "00000" "00000",
			"20000" "02000" "00200" "00020" "00004",
			"30000" "02000" "00200" "00040" "00002",
			//low threats
			"20000" "02000" "00200" "00040" "00000",
			"00000" "00000" "02224" "00000" "00000",
			//3 allies
			"00000" "00000" "01114" "00000" "00000",
			"00000" "01000" "00100" "00010" "00004",
			//2 allies
			"00000" "00000" "00114" "00000" "00000",
			"00000" "00000" "00100" "00010" "00004",
			//1 ally
			"00000" "00000" "00140" "00000" "00000",
			"00000" "00000" "00100" "00040" "00000",
			//1 enemy
			"00000" "00000" "00240" "00000" "00000",
			"00000" "00000" "00200" "00040" "00000",
	};

	for (auto &pattern : patterns) {
		auto board = Board(5, pattern);

		_patterns.emplace_back(board);
		_patterns.emplace_back(_patterns.back().turnBoard());
		_patterns.emplace_back(_patterns.back().turnBoard());
		_patterns.emplace_back(_patterns.back().turnBoard());
	}
}

Position AI::play()
{
	size_t index = 0;
	int maxScore = -1;
	int tmp;
/*	std::vector<Pattern> patternsFound = searchMatchingPattern();
	Position absolute;

	if (patternsFound.empty()) {
		absolute.x = std::rand() % _board->getSize();
		absolute.y = std::rand() % _board->getSize();
		while (_board->getBox(absolute) != 0)
		{
			absolute.x = std::rand() % _board->getSize();
			absolute.y = std::rand() % _board->getSize();
		}
		return absolute;
	} else {
		absolute.x = patternsFound[0].position.x;
		absolute.y = patternsFound[0].position.y;
		Position relative;
		relative = patternsFound[0].patternBoard.getMove();
		absolute.x += relative.x - 2;
		absolute.y += relative.y - 2;
		return absolute;
	}*/
	generateGraph();
	for (size_t i = 0; i < _root->next.size(); i++) {
		tmp = getBestBranch(*_root->next[i]);
		if (tmp > maxScore){
			maxScore = tmp;
			index = i;
		}
	}
	if (maxScore == -1) {
		Position absolute;
		absolute.x = std::rand() % _board->getSize();
		absolute.y = std::rand() % _board->getSize();
		while (_board->getBox(absolute) != 0)
		{
			absolute.x = std::rand() % _board->getSize();
			absolute.y = std::rand() % _board->getSize();
		}
		return absolute;
	} else {
		return (*_root->next[index]).playedPosition;
	}
}

std::vector<Pattern> AI::simulate(Board &board)
{
	std::vector<Pattern> patternsFound = searchMatchingPattern();

	return patternsFound;
}

std::vector<Pattern> AI::searchMatchingPattern()
{
	Pattern null;
	std::vector<Pattern> positions;
	size_t score = _patterns.size();

	for (auto &pattern : _patterns) {
		for (auto &position : _board->getPawns()) {
			if (patternMatches(pattern, position)) {
				positions.emplace_back(Pattern(pattern, position, score));
			}
		}
		score--;
	}
	return positions;
}

std::vector<Pattern> AI::searchMatchingPattern(Board &board, const bool enemy)
{
	Pattern null;
	std::vector<Pattern> positions;
	size_t score = _patterns.size();

	for (auto &pattern : _patterns) {
		for (auto &position : board.getPawns()) {
			if (patternMatches(pattern, position, enemy)) {
				positions.emplace_back(Pattern(pattern, position, score));
			}
		}
		score--;
	}
	return positions;
}

void AI::setBoard(std::shared_ptr<Board> newBoard)
{
	_board = std::move(newBoard);
}

Position AI::simulateEnemy(Board &board)
{
	std::vector<Pattern> patternsFound = searchMatchingPattern(board, true);
	Position absolute;

	if (patternsFound.empty()) {
		absolute.x = std::rand() % board.getSize();
		absolute.y = std::rand() % board.getSize();
		while (board.getBox(absolute) != 0)
		{
			absolute.x = std::rand() % board.getSize();
			absolute.y = std::rand() % board.getSize();
		}
		return absolute;
	} else {
		absolute.x = patternsFound[0].position.x;
		absolute.y = patternsFound[0].position.y;
		Position relative;
		relative = patternsFound[0].patternBoard.getMove();
		absolute.x += relative.x - 2;
		absolute.y += relative.y - 2;
		return absolute;
	}
}

size_t AI::getBestBranch(Node &node)
{
	size_t score = 0;
	size_t tmp;
	for (auto &childNode : node.next) {
		tmp = getBestBranch(*childNode);
		if (tmp > score)
			score = tmp;
	}
	return score;
}

std::unique_ptr<Node>  AI::generateGraph(Board &board, size_t score, size_t deepness, Position posPlayed)
{
	auto node = std::make_unique<Node>(board);
	auto thickNess = 3;
	Position enemy;
	node->playedPosition = posPlayed;
	node->score = score;

	if (deepness >= 4) {
		return node;
	}
	if (deepness > 1) {
		enemy = simulateEnemy(board);
		board.setBoxSave(enemy, ENEMY);
	}
	auto vector = searchMatchingPattern(board);
	auto i = 0;
	for (const auto &pattern : vector) {
		if (i == thickNess) {
			break;
		}
		Position newPos;
		Position relative;
		Position enemyMove;
		Board newBoard = board;

		newPos.x = pattern.position.x;
		newPos.y = pattern.position.y;
		relative = pattern.patternBoard.getMove();
		newPos.x += relative.x - 2;
		newPos.y += relative.y - 2;
		newBoard.setBoxSave(newPos, ALLY);
		node->next.push_back(generateGraph(newBoard, pattern.score, deepness + 1, newPos));
		i++;
	}
	return node;
}

void AI::generateGraph()
{
	_root = generateGraph(*_board);
}
