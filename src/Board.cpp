
#include <Board.hpp>

#include "Board.hpp"

Board::Board(const int size = 20)
	: _sizeBoard(static_cast<const size_t>(size)),
	_array(_sizeBoard * _sizeBoard)
{
	std::fill(_array.begin(), _array.end(), 0);
}

Board::Board(int size, std::string board) : _sizeBoard(static_cast<const size_t>(size))
{
	for (auto &c : board) {
		_array.push_back(static_cast<unsigned int>(c - '0'));
	}
}

Board::~Board() = default;

size_t Board::getSize() const
{
	return _sizeBoard;
}

int Board::getBox(const Position &position) const
{
	return _array[position.y * _sizeBoard + position.x];
}

void Board::setBox(const Position &position, const int value)
{
	_array[position.y * _sizeBoard + position.x] = value;
}

std::vector<Position> Board::getPawns() const
{
	return _pawns;
}

Position Board::getMove() const
{
	Position relative(-1, -1);

	for (size_t i = 0; i < _array.size(); ++i) {
		if (_array[i] == 4) {
			relative.x = i % _sizeBoard;
			relative.y = i / _sizeBoard;
			return relative;
		}
	}
	return relative;
}

void Board::setBoxSave(Position position, int value)
{
	Position pos;

	_array[position.y * _sizeBoard + position.x] = value;
	pos.x = position.x;
	pos.y = position.y;
	_pawns.push_back(pos);
}

Board Board::turnBoard() {
	Board turned(5);

	const size_t N = getSize();
	// An Inplace function to rotate a N x N matrix
	// by 90 degrees in anti-clockwise direction
	// Consider all squares one by one
	for (size_t x = 0; x < N / 2; x++)
	{
		// Consider elements in group of 4 in
		// current square
		for (size_t y = x; y < N - x - 1; y++)
		{
			// store current cell in temp variable
			int temp = getBox(Position(x, y));

			// move values from right to top
			turned.setBox(Position(x, y), getBox(Position(y, N - 1 - x)));

			// move values from bottom to right
			turned.setBox(Position(y, N - 1 - x), getBox(Position(N - 1 - x, N - 1 - y)));

			// move values from left to bottom
			turned.setBox(Position(N - 1 - x, N - 1 - y), getBox(Position(N - 1 - y, x)));
			// assign temp to left
			turned.setBox(Position(N - 1 - y, x), temp);
		}
	}
	return turned;
}
