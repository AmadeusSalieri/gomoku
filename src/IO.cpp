//
// Created by Kazuhiro on 09/10/2018.
//

#include <IO.hpp>
#include <iostream>

#include "IO.hpp"

IO::IO(std::string &data) : _data(data) {

}

const std::string &IO::readInput() {
    _data.clear();
	std::getline(std::cin, _data);
    return _data;
}

void IO::out(const std::string &message) {
	std::cout << message << std::endl;
}

void IO::setFile(const std::string &file) {
	_file.open(file);
}
void IO::writeBrain(const std::string &message) {
	_file << "Brain : " << message << std::endl;
}

void IO::writeManager(const std::string &message) {
	_file << "Manager : " << message << std::endl;
}

void IO::closeFile() {
	_file.close();
}